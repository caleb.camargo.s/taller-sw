/**
 * @file Problema5.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 5
 * @version 1.0
 * @date 02/03/2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float side1;
float side2;
float side3;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void calculus();
void collect_Data();
void type_of_triangle(float , float, float);
void Run();
bool triangle_existence_confirmation(float , float, float);
bool triangle_equilateral_confirmation(float , float, float);
bool triangle_isoceles_confirmation(float , float, float);
float absolutevalue(float );
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
int main(){
Run();
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void collect_Data(){
    cout<<"Enter the first side of the triangle: ";
    cin>>side1;
    cout<<"Enter the second side of the triangle: ";
    cin>>side2;
    cout<<"Enter the third side of the triangle: ";
    cin>>side3;
}
float absolutevalue(float n){
    if(n<=0.0){
        return -1.0*n;
    }
    else{
        return n;
    }
}
bool triangle_existence_confirmation(float a, float b, float c){
    if((absolutevalue(a-c)<b) && (b<a+c)){
        if((absolutevalue(a-b)<c) && (c<a+b)){
            if((absolutevalue(b-c)<a) && (a<b+c)){
                return true;
            }
        }
    }
    return false;
}
bool triangle_equilateral_confirmation(float a,float b, float c){
    if ((a==b) && (b==c)){
    return true;
    }
    return false;
}
bool triangle_isoceles_confirmation(float a,float b, float c){
    if((a==b)||(b==c)||(c==a)){
        return true;
    }
    return  false;
}
void type_of_triangle(float a , float b , float c){
    if(triangle_equilateral_confirmation(side1,side2,side3)){
        cout<<"Is a Equilateral Triangle"<<endl;
    }
    else if(triangle_isoceles_confirmation(side1,side2,side3)){
        cout<<"Is a Isoceles Triangle"<<endl;
    }
    else{
        cout<<"Is a Scalene Triangle"<<endl;
    }
}
void calculus(){
    if(triangle_existence_confirmation(side1,side2,side3)){
        type_of_triangle(side1,side2,side3);
    }
    else{
        cout<<"Doesn't exist a triangle with those sides";
    }
}
void Run(){
    collect_Data();
    calculus();
    getch();
}