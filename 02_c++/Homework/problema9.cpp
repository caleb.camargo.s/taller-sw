/**
 * @file Exercise9.cpp
 * @author Caleb Camargo(caleb.camargo.s@uni.pe)
 * @brief exercise 9
 * @version 1.0
 * @date 02/03/2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float totalSales=0.0;
float firstFactorCommission=0.1;
float secondFactorCommission=0.09;
float secondBonusCommission=50;
float totalCommission=0.0;
int numberCommission=0;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int GetNumberCommmission(float sales);
float CalculateTotalComission(int numberCode, float sales);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Insert total sales: ";
	cin>>totalSales;
}
//=====================================================================================================

void Calculate(){
	numberCommission=GetNumberCommmission(totalSales);
	totalCommission=CalculateTotalComission(numberCommission,totalSales);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The total commission is: "<<totalCommission;
}
//=====================================================================================================

int GetNumberCommmission(float sales){
	int number;
	if (sales<=150){
		number=0;
	}else if(sales>150 && sales<=400){
		number=1;
	}else{
		number=2;
	}
	return number;
}

//=====================================================================================================

float CalculateTotalComission(int numberCode, float sales){
	float commission;
	switch (numberCode){
		case 0:
			commission=0;
		break;
		case 1:
			commission=firstFactorCommission*sales;
		break;
		case 2:
			commission=secondFactorCommission*sales+secondBonusCommission;
		break;
			
	}
	return commission;
}

