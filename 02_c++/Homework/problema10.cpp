/**
 * @file Exercise10.cpp
 * @author Caleb Camargo (caleb.camargo.s@uni.pe)
 * @brief exercise 10
 * @version 1.0
 * @date 02/03/2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float mFactorLine=0.0;
float cConstantLine=0.0;
float aConstantCircle=0.0;
float bConstantCircle=0.0;
float rRadiusCircle=0.0;
float discriminant=0.0;
string typeLine;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateDiscriminant(float a, float b, float m, float c, float r);
string GetTypeLine(float delta);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"\tEc 1: y=mx+c\n\tEc 2: (x-a)^2+(y-b)^2=r^2";
	cout<<"\nIn the first equation:";
	cout<<"\n\tInsert m:  ";
	cin>>mFactorLine;
	cout<<"\n\tInsert c:  ";
	cin>>cConstantLine;
	cout<<"\nIn the second equation:";
	cout<<"\n\tInsert a:  ";
	cin>>aConstantCircle;
	cout<<"\n\tInsert b:  ";
	cin>>bConstantCircle;
	cout<<"\n\tInsert circle radius 'r':  ";
	cin>>rRadiusCircle;
}
//=====================================================================================================

void Calculate(){
	discriminant=CalculateDiscriminant(aConstantCircle,bConstantCircle,mFactorLine,cConstantLine,rRadiusCircle);
	typeLine=GetTypeLine(discriminant);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"The line is: "<<typeLine;
}
//=====================================================================================================

float CalculateDiscriminant(float a, float b, float m, float c, float r){
	float delta;
	float constB=2*m*c-2*a-2*m*b;
	float constA=1+pow(m,2);
	float constC=pow(a,2)+pow(b,2)+pow(c,2)-2*c*b-pow(r,2);
	delta=pow(constB,2)-4*constA*constC;
	return delta;
}
string GetTypeLine(float delta){
	string type;
	if (delta==0){
		type="Tangent";
	}else if(delta<0){
		type="Does not intersect";
	}else{
		type="Secant";
	}
	return type;
}

//=====================================================================================================

