/**
 * @file Problema3.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 3
 * @version 1.0
 * @date 04/02/2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float number_1;
float number_2;
float number_3;
float Minimun_number;
float Midle_number;
float maximun_number;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void calculus();
void collect_Data();
void Show_results();
void Run();
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
int main(){
Run();
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void collect_Data(){
	cout<<"enter the first number: ";
	cin>>number_1;
    cout<<"enter the second number: ";
	cin>>number_2;
    cout<<"enter the third number: ";
	cin>>number_3;
}
void Show_results(){
	cout<<"The final numbers in order : "<<Minimun_number<<" "<<Midle_number<<" "<<maximun_number;	
}
void Run(){
	collect_Data();
	calculus();
	Show_results();
}
void calculus(){
    Minimun_number=min(number_1,min(number_2,number_3));
    maximun_number=max(number_1,max(number_2,number_3));
    Midle_number=number_1+number_2+number_3-Minimun_number-maximun_number;
}