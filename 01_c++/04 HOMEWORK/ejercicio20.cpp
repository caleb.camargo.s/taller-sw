/**
 * @file CodeReviewExercise20.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 20
 * @version 1.0
 * @date 28.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;
#define PI 3.1415926

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float ladoInputAB=0.0;
float ladoInputAC=0.0;
float anguloInputA=0.0;
double ladoCalcularBC=0;
double anguloCalcularB=0;
double anguloCalcularC=0;
double areaTriangulo=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularladoBC(float ladoa,float ladob,float angulo);
double CalcularAngulo(double ladoOpuesto);
double CalcularArea(float ladoa,float ladob,float angulo);


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese la longitud del lado AB: ";
	cin>>ladoInputAB;
	cout<<"Ingrese la longitud del lado AC: ";
	cin>>ladoInputAC;
	cout<<"Ingrese el angulo A: ";
	cin>>anguloInputA;
}
//=====================================================================================================

void Calculate(){
	ladoCalcularBC=CalcularladoBC(ladoInputAB,ladoInputAC,anguloInputA);
	anguloCalcularB=CalcularAngulo(ladoInputAC);
	anguloCalcularC=CalcularAngulo(ladoInputAB);
	areaTriangulo=CalcularArea(ladoInputAB,ladoInputAC,anguloInputA);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El tercer lado o lado BC mide: ";
	cout<<"\n\t"<<ladoCalcularBC;
	cout<<"\nEl angulo B es: ";
	cout<<"\n\t"<<anguloCalcularB;
	cout<<"\nEl angulo C es: ";
	cout<<"\n\t"<<anguloCalcularC;
	cout<<"\nEl area del triangulo es: ";
	cout<<"\n\t"<<areaTriangulo;
}
//=====================================================================================================

double CalcularladoBC(float ladoa,float ladob,float angulo){
	angulo=angulo*PI/180;
	double lado=sqrt(pow(ladoa,2)+pow(ladob,2)-2*ladoa*ladob*cos(angulo));
	return lado;
}
//=====================================================================================================

double CalcularAngulo(double ladoOpuesto){
	double cosAngulo=
	
}
//=====================================================================================================

double CalcularArea(float ladoa,float ladob,float angulo){
	angulo=angulo*PI/180;
	double area=ladoa*ladob*sin(angulo)/2;
	return area;
	
}
//=====================================================================================================
