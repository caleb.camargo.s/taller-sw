/**
 * @file CodeReviewExercise4.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 4
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float temperature_celcius;//temperature in celcius
float temperature_farentheid;//temperature in farentheid
float inches_water;//amount of water in inches
float millimeters_water;//amount of water in millimeters
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
 void Collect_Temperature();
 void Collect_water();
 void Show_final_Temperature();
 void Show_final_water();
 void Run();
 void Calculate_temperature_farentheid(float);
 void Calculate_millimeters_water(float);
 
 /*******************************************************************************************************************************************
 *  												MAIN FUNCTION
 *******************************************************************************************************************************************/
int main(){

	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINICION
 *******************************************************************************************************************************************/
void Collect_Temperature(){
	cout<<"Digite la temperatura en celcius: ";
	cin>>temperature_celcius;
}
void Collect_water(){
	cout<<"Digite la cantidad de agua en pulgadas: ";
	cin>>inches_water;
}
void Show_final_Temperature(){
	cout<<"La temperatura en Farentheid es: "<<temperature_farentheid<<endl;
}
void Show_final_water(){
	cout<<"Digite la cantidad de agua en Milimetros: "<<millimeters_water;
}
void Calculate_temperature_farentheid(float t_c){
	temperature_farentheid=t_c*1.8+32.0;
}
void Calculate_millimeters_water(float i_w){
	millimeters_water=i_w*25.5;
	
}
void Run(){
	Collect_Temperature();
	Calculate_temperature_farentheid(temperature_celcius);
	Show_final_Temperature();
	Collect_water();
	Calculate_millimeters_water(inches_water);
	Show_final_water();
}
 
