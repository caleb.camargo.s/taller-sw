/**
 * @file CodeReviewExercise11.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 11
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float meters = 0;	//longitude in meters
 	float yards=0; //longitude in yards
    float inch=0; //longitude in inch
    float feets=0; //longitude in feets
    float centimeters; //longitude in centimeters

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the longitude in meters:"<<endl;
 	cin>>meters;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    centimeters=meters*100;
    inch=centimeters/2.54;
    feets=inch/12;
    yards=feets/3;

}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The longitude in centimeters:  "<<centimeters<<endl;
    cout<<"The longitude in yards:  "<<yards<<endl;
    cout<<"The longitude in feets:  "<<feets<<endl;
    cout<<"The longitude in inch:  "<<inch<<endl;
}