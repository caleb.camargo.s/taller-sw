/**
 * @file CodeReviewExercise10.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 10
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float carlos = 0;	//inheritance of carlos
 	float jose=0; //inheritance of jose
    float martha=0; //inheritance of martha
    float total_of_money=0; //fortune of father

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the fortune of the father of three sons:"<<endl;
 	cin>>total_of_money;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    carlos=total_of_money/3;
    jose=carlos*4/3;
    martha=jose/2;

}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The fortune was:  "<<total_of_money<<endl;
    cout<<"inheritance of jose:  "<<jose<<endl;
    cout<<"inheritance of carlos:  "<<carlos<<endl;
    cout<<"inheritance of martha:  "<<martha<<endl;
}