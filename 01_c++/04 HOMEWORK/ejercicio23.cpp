/**
 * @file CodeReviewExercise23.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 23
 * @version 1.0
 * @date 28.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
#define PI 3.1415926
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float anguloInputA=0.0;
float anguloInputB=0.0;
float anguloC=0.0;
float ladoInputC=0.0;
double ladoCalcularB=0;
double ladoCalcularA=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalcularLongitudLado(float anguloOpuesto);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el angulo alpha: ";
	cin>>anguloInputA;
	cout<<"Ingrese el angulo betha: ";
	cin>>anguloInputB;
	cout<<"Ingrese el lado c: ";
	cin>>ladoInputC;
}
//=====================================================================================================

void Calculate(){
	anguloC=180-anguloInputA-anguloInputB;
	ladoCalcularA=CalcularLongitudLado(anguloInputA);
	ladoCalcularB=CalcularLongitudLado(anguloInputB);
	
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El lado a mide:  ";
	cout<<"\n\t"<<ladoCalcularA;
	cout<<"\nEl lado b mide: ";
	cout<<"\n\t"<<ladoCalcularB;
}
//=====================================================================================================

double CalcularLongitudLado(float anguloOpusto){
	double r=anguloC;
	r=r*PI/180;
	anguloOpusto=anguloOpusto*PI/180;
	double a=ladoInputC/sin(r);
	double lado=a*sin(anguloOpusto);
	return lado;
	
}
//=====================================================================================================
