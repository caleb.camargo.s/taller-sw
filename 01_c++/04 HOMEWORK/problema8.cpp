/**
 * @file CodeReviewExercise8.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 8
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float mount = 0;	//final mount
 	float capital=0; //initial capital
    float time_years=0; //total time in years
    float n_periods_year=0; //number of periods for year
    float rate=0; //rate for years

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the capital:"<<endl;
 	cin>>capital;
    cout<<"\tEnter the rate:"<<endl;
 	cin>>rate;
    cout<<"\tEnter the total period"<<endl;
 	cin>>time_years;
    cout<<"\tEnter the total period"<<endl;
 	cin>>n_periods_year;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    mount=capital*pow(1+(rate/n_periods_year),(time_years*n_periods_year));
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The final mount is:  "<<mount;
}
//1)19990.1
//2)126923