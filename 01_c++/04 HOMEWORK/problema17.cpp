/**
 * @file CodeReviewExercise17.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 17
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float small_radius = 0;	//small radius
 	float large_radius =0; //large radius
    float ellipse_area =0; //area of ellipse
    float ellipse_perimeter =0; //perimeter of perimeter
    float PI=3.1416; //constant

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the small radius:"<<endl;
 	cin>>small_radius;
    cout<<"\tEnter the large radius:"<<endl;
 	cin>>large_radius;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    ellipse_area=PI*small_radius*large_radius;
    ellipse_perimeter=2*PI*sqrt((pow(small_radius,2)+pow(large_radius,2))/2);

}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The Area is:  "<<ellipse_area<<endl;
    cout<<"The Perimeter is:  "<<ellipse_perimeter<<endl;
}