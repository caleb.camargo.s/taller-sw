/**
 * @file CodeReviewExercise15.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 15
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
float x1 = 0;	//x coordinate of point one
int y1 = 0; //y coordinate of point one
float x2 = 0; //x coordinate of point two
float y2 = 0;// y coordinate of point two
float medium_point_x = 0;//x coordinate of midle point between p1 and p2
float medium_point_y = 0;//y coordinate of midle point between p1 and p2
float slope = 0;//slope between p1 p2
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the x coodinate of point one:"<<endl;
 	cin>>x1;
    cout<<"\tEnter the y coodinate of point one:"<<endl;
 	cin>>y1;
    cout<<"\tEnter the x coodinate of point two:"<<endl;
 	cin>>x2;
 	cout<<"\tEnter the y coodinate of point two:"<<endl;
 	cin>>y2;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    slope=(y2-(float)y1)/(x2-x1);
    medium_point_x=(x1+x2)/2;
    medium_point_y=((float)y1+y2)/2;
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The slove is:  "<<slope<<"cm3"<<endl;
    cout<<"The midle point is:  ("<<medium_point_x<<","<<medium_point_y<<")"<<endl;
}
//1 y (5.5,9.5)