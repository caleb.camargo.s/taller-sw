/**
 * @file CodeReviewExercise7.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 7
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float mass1 = 0;	//mass of first planet
 	float mass2=0; //mass of second planet
    float longitude=0; //longitude between
    float G=6.673e-8; //constant
    float strength=0; //strength of atraction

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the mass of first planet:"<<endl;
 	cin>>mass1;
    cout<<"\tEnter the mass of second planet:"<<endl;
 	cin>>mass2;
    cout<<"\tEnter the longitude between this planets:"<<endl;
 	cin>>longitude;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    strength=(G*mass1*mass2)/(pow(longitude,2));
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The strength of atraction is:  "<<strength;
}