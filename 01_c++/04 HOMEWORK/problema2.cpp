/**
 * @file CodeReviewExercise2.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 2
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int men=0;//number of men
int women=0;//number of women
int total=0;//number of total people
float percentage_men=0.0;//percentage of men
float percentage_women=0.0;//percentage of women


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void collect_Data();
float Calculate_Porcentage(int,int);
void Calculus(int,int);
void Show_Results();
void Run();
/*******************************************************************************************************************************************
 *  												MAIN FUNCTION
 *******************************************************************************************************************************************/

int main(){

	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void collect_Data(){
	cout<<"Digite el numero de hombres: ";
	cin>>men;
	cout<<"Digite el numero de mujeres: ";
	cin>>women;
}
float Calculate_Porcentage(int a,int b){
	float value;
	value=((float)a/(float)(b))*100;
	return value;
}
void Calculus(int m,int w){
	total=m+w;
	percentage_men=Calculate_Porcentage(men,total);
	percentage_women=Calculate_Porcentage(women,total);
}
void Show_Results(){
	cout<<"El porcentaje de hombres es: "<<percentage_men<<"%"<<endl;
	cout<<"El porcentaje de mujeres es: "<<percentage_women<<"%"<<endl;
	
}
void Run(){
	collect_Data();
	Calculus(men,women);
	Show_Results();
}
