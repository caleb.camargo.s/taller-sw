
/**
 * @file CodeReviewExercise1.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 1
 * @version 1.0
 * @date 28.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float radio_base;//radio of base of cylinder
float height_cylinder;//height of cylinder
float lateral_area_cylinder;//lateral area of cylinder
float volume_cylinder;//volume of cylinder
float PI=3.1416;//constant pi
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void calculus(float ,float);
void collect_Data();
void Show_results();
void calculate_lateral_area_cylinder(float ,float );
void calculate_volume_cylinder(float ,float);
void Run();
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
int main(){
Run();
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void calculus(float r_b,float h_c){
	calculate_volume_cylinder(r_b,h_c);
	calculate_lateral_area_cylinder(r_b, h_c);
}
void collect_Data(){
	cout<<"Digite el radio de la base: ";
	cin>>radio_base;
	cout<<"Digite la altura de la base: ";
	cin>>height_cylinder;
}
void Show_results(){
	cout<<"El area lateral del cilindro es: "<<lateral_area_cylinder<<endl;
	cout<<"El volumen del cilindro es:"<<volume_cylinder<<endl;
	
}
void calculate_lateral_area_cylinder(float r_b,float h_c){
	lateral_area_cylinder=2.0*PI*r_b*h_c;
}
void calculate_volume_cylinder(float r_b,float h_c){
	volume_cylinder=PI * (pow(r_b,2)) * h_c;
}
void Run(){
	collect_Data();
	calculus(radio_base,height_cylinder);
	Show_results();
}


