/**
 * @file CodeReviewExercise14.cpp
 * @author Caleb Tomas Camargo Saavedra (caleb.camargo.s@uni.pe)
 * @brief exercise 14
 * @version 1.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
//Declaration e initialization of the variables
 	float small_diameter = 0;	//smaller diameter
 	float large_diameter=0; //larger diameter
    float barrel_length=0; //barrel length
    float height=0;// heihgt of barrel
    float capacity;//capacity of barrel
    float PI=3.1426;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the largest diameter in centimeters:"<<endl;
 	cin>>large_diameter;
    cout<<"\tEnter the smaller diameter in centimeters:"<<endl;
 	cin>>small_diameter;
    cout<<"\tEnter the barrel length in centimeters:"<<endl;
 	cin>>barrel_length;
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
    height= small_diameter/2 + (2/3)*(large_diameter/2 - small_diameter/2);
    capacity=PI*barrel_length*pow(height,2);

}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The total capacity of barrel is:  "<<capacity<<"cm3"<<endl;
    cout<<"The total capacity of barrel is:  "<<capacity*0.001<<"L"<<endl;
    cout<<"The total capacity of barrel is:  "<<capacity/1000000<<"m3"<<endl;
}